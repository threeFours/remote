package com.remote.aschl.remote;

import android.content.Context;
import android.os.AsyncTask;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.DataOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

    private char controller;
    private Vibrator vibe;
    private TextView sourceTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        controller = 'h';
        sourceTextView = (TextView) findViewById(R.id.sourceText);
    }

    public void sendPiMessage(View view) {
        Object arg;

        vibe.vibrate(VibrationEffect.createOneShot(50, 100));
        switch(view.getId()){
            case R.id.powerButton:
                Object arg2;
                arg = "IR|irsend SEND_ONCE tv KEY_POWER";
                arg2 = "IR|irsend SEND_ONCE receiver KEY_POWER";
                new sendToPi().execute(arg);
                new sendToPi().execute(arg2);
                break;
            case R.id.cableButton:
                controller = 'c';
                sourceTextView.setText("Cable");
                arg = "IR|irsend SEND_ONCE receiver KEY_1";
                new sendToPi().execute(arg);
                break;
            case R.id.htpcButton:
                controller = 'h';
                sourceTextView.setText("HTPC");
                arg = "IR|irsend SEND_ONCE receiver KEY_2";
                new sendToPi().execute(arg);
                break;
            case R.id.plexButton:
                controller = 'p';
                sourceTextView.setText("Plex");
                break;
            case R.id.rateButton:
                controller = 't';
                sourceTextView.setText("TV");
                arg = "IR|irsend SEND_ONCE tv KEY_MODE";
                new sendToPi().execute(arg);
                break;
            case R.id.volupButton:
                arg = "IR|irsend SEND_ONCE receiver KEY_VOLUMEUP";
                new sendToPi().execute(arg);
                break;
            case R.id.voldownButton:
                arg = "IR|irsend SEND_ONCE receiver KEY_VOLUMEDOWN";
                new sendToPi().execute(arg);
                break;
            case R.id.muteButton:
                arg = "IR|irsend SEND_ONCE receiver KEY_MUTE";
                new sendToPi().execute(arg);
                break;
            case R.id.infoButton:
                if (controller == 'h') {
                    sendToPlex("http://192.168.1.15:3005/player/navigation/contextMenu");
                    break;
                } else {
                    arg = "NA|echo 'no controller'";
                }
                new sendToPi().execute(arg);
                break;
            case R.id.osdButton:
                if (controller == 'p') {
                    sendToPlex("http://192.168.1.15:3005/player/navigation/toggleOSD");
                    break;
                } else {
                    arg = "NA|echo 'no controller'";
                }
                new sendToPi().execute(arg);
                break;
            case R.id.bearsButton:
                arg = "GM|bears";
                new sendToPi().execute(arg);
                break;
            case R.id.bullsButton:
                arg = "GM|bulls";
                new sendToPi().execute(arg);
                break;
            case R.id.hawksButton:
                arg = "GM|hawks";
                new sendToPi().execute(arg);
                break;
            case R.id.cubsButton:
                arg = "GM|cubs";
                new sendToPi().execute(arg);
                break;
            case R.id.upButton:
                if (controller == 't') {
                    arg = "IR|irsend SEND_ONCE tv_non_raw KEY_UP";
                } else if (controller == 'h') {
                    sendToPlex("http://192.168.1.15:3005/player/navigation/moveUp");
                    break;
                } else if (controller == 'c'){
                    arg = "IRCODE UP";
                    new sendToCable().execute(arg);
                    break;
                } else if (controller == 'p') {
                    sendToPlex("http://192.168.1.15:3005/player/playback/bigStepForward");
                    break;
                } else {
                    arg = "NA|echo 'no controller'";
                }
                new sendToPi().execute(arg);
                break;
            case R.id.downButton:
                if (controller == 't') {
                    arg = "IR|irsend SEND_ONCE tv_non_raw KEY_DOWN";
                } else if (controller == 'h') {
                    sendToPlex("http://192.168.1.15:3005/player/navigation/moveDown");
                    break;
                } else if (controller == 'c') {
                    arg = "IRCODE DOWN";
                    new sendToCable().execute(arg);
                    break;
                } else if (controller == 'p') {
                    sendToPlex("http://192.168.1.15:3005/player/playback/bigStepBack");
                    break;
                } else {
                    arg = "NA|echo 'no controller'";
                }
                new sendToPi().execute(arg);
                break;
            case R.id.leftButton:
                if (controller == 't') {
                    arg = "IR|irsend SEND_ONCE tv_non_raw KEY_LEFT";
                } else if (controller == 'h') {
                    sendToPlex("http://192.168.1.15:3005/player/navigation/moveLeft");
                    break;
                } else if (controller == 'c') {
                    arg = "IRCODE LEFT";
                    new sendToCable().execute(arg);
                    break;
                } else if (controller == 'p') {
                    sendToPlex("http://192.168.1.15:3005/player/playback/stepBack");
                    break;
                } else {
                        arg = "NA|echo 'no controller'";
                }
                new sendToPi().execute(arg);
                break;
            case R.id.rightButton:
                if (controller == 't') {
                    arg = "IR|irsend SEND_ONCE tv_non_raw KEY_RIGHT";
                } else if (controller == 'h') {
                    sendToPlex("http://192.168.1.15:3005/player/navigation/moveRight");
                    break;
                } else if (controller == 'c'){
                    arg = "IRCODE RIGHT";
                    new sendToCable().execute(arg);
                    break;
                } else if (controller == 'p') {
                    sendToPlex("http://192.168.1.15:3005/player/playback/stepForward");
                    break;
                } else {
                    arg = "NA|echo 'no controller'";
                }
                new sendToPi().execute(arg);
                break;
            case R.id.selectButton:
                if (controller == 't') {
                    arg = "IR|irsend SEND_ONCE tv_non_raw KEY_SELECT";
                } else if (controller == 'h') {
                    sendToPlex("http://192.168.1.15:3005/player/navigation/select");
                    break;
                } else if (controller == 'c'){
                    arg = "IRCODE SELECT";
                    new sendToCable().execute(arg);
                    break;
                } else if (controller == 'p') {
                    sendToPlex("http://192.168.1.15:3005/player/playback/pause");
                    break;
                } else {
                    arg = "NA|echo 'no controller'";
                }
                new sendToPi().execute(arg);
                break;
            case R.id.backButton:
                if (controller == 't') {
                    arg = "IR|irsend SEND_ONCE tv_non_raw KEY_BACK";
                } else if (controller == 'h') {
                    sendToPlex("http://192.168.1.15:3005/player/navigation/back");
                    break;
                } else if (controller == 'c') {
                    arg = "IRCODE EXIT";
                    new sendToCable().execute(arg);
                    break;
                } else {
                    arg = "NA|echo 'no controller'";
                }
                new sendToPi().execute(arg);
                break;
            case R.id.pageUpButton:
                if (controller == 'h') {
                    //sendToPlex("http://192.168.1.15:3005/player/navigation/back");
                    break;
                } else if (controller == 'c') {
                    arg = "IRCODE CHANNELUP";
                    new sendToCable().execute(arg);
                    break;
                } else {
                    arg = "NA|echo 'no controller'";
                }
                new sendToPi().execute(arg);
                break;
            case R.id.pageDownButton:
                if (controller == 'h') {
                    //sendToPlex("http://192.168.1.15:3005/player/navigation/back");
                    break;
                } else if (controller == 'c') {
                    arg = "IRCODE CHANNELDOWN";
                    new sendToCable().execute(arg);
                    break;
                } else {
                    arg = "NA|echo 'no controller'";
                }
                new sendToPi().execute(arg);
                break;
            case R.id.lastButton:
                new sendToCable().execute("IRCODE ENTER");
            case R.id.guideButton:
                controller = 'c';
                sourceTextView.setText("Cable");
                new sendToCable().execute("IRCODE GUIDE");
        }
    }

    private class sendToCable extends AsyncTask<Object, Void, Void>{
        @Override
        protected Void doInBackground(Object... params) {
            try {
                Socket s = new Socket("192.168.1.10", 31339);
                PrintWriter outToServer = new PrintWriter(
                        new OutputStreamWriter(s.getOutputStream()), true);
                Log.i("sendToCable", params[0].toString());
                outToServer.println(params[0].toString()+"\r\n\r\n");
                s.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private class sendToPi extends AsyncTask<Object, Void, Void>{
        @Override
        protected Void doInBackground(Object... params) {
            try {
                Socket s = new Socket("192.168.1.173", 9090);
                DataOutputStream outToServer = new DataOutputStream(s.getOutputStream());
                Log.i("sendToPi", params[0].toString());
                outToServer.writeBytes(params[0].toString());
                s.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private void sendToPlex(String url){
        RequestQueue queue = Volley.newRequestQueue(this);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("That didn't work!");
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
